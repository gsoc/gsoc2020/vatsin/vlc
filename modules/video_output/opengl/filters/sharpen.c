/*****************************************************************************
 * sharpen.c : Sharpen GPU video filter
 *****************************************************************************
 * Copyright (C) 2020 VLC authors and VideoLAN
 * Copyright (C) 2020 Videolabs
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdatomic.h>
#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_modules.h>
#include <vlc_opengl.h>
#include <vlc_filter.h>

#include "../filter.h"
#include "../gl_api.h"
#include "../gl_common.h"
#include "../gl_util.h"

#define FILTER_PREFIX "sharpen-"

struct sys {
    GLuint program_id;

    GLuint vbo;

    struct {
        GLint vertex_pos;
        GLint sigma;
    } loc;
	_Atomic float sigma;
};

static const char *const ppsz_filter_options[] = {
    "sigma", NULL
};

static int varFloatCallback(vlc_object_t *p_this, char const *psz_variable, 
	                 vlc_value_t oldvalue, vlc_value_t newvalue, void *p_data) {
	_Atomic float *atom = p_data;
	atomic_store_explicit(atom, newvalue.f_float, memory_order_relaxed);
	(void) p_this; (void) psz_variable; (void) oldvalue;
    return VLC_SUCCESS;
}

static int
Draw(struct vlc_gl_filter *filter, const struct vlc_gl_input_meta *meta)
{
    struct sys *sys = filter->sys;

    const opengl_vtable_t *vt = &filter->api->vt;

    vt->UseProgram(sys->program_id);

    struct vlc_gl_sampler *sampler = vlc_gl_filter_GetSampler(filter);
    vlc_gl_sampler_Load(sampler);

    vt->Uniform1f(sys->loc.sigma, atomic_load_explicit(&sys->sigma, memory_order_relaxed));

    vt->BindBuffer(GL_ARRAY_BUFFER, sys->vbo);
    vt->EnableVertexAttribArray(sys->loc.vertex_pos);
    vt->VertexAttribPointer(sys->loc.vertex_pos, 2, GL_FLOAT, GL_FALSE, 0,
                            (const void *) 0);

    vt->Clear(GL_COLOR_BUFFER_BIT);
    vt->DrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    return VLC_SUCCESS;
}

static void
Close(struct vlc_gl_filter *filter) {
    struct sys *sys = filter->sys;

    const opengl_vtable_t *vt = &filter->api->vt;
    vt->DeleteProgram(sys->program_id);
    vt->DeleteBuffers(1, &sys->vbo);
    var_DelCallback( filter, FILTER_PREFIX "sigma", varFloatCallback, 
    	             &sys->sigma );
    free(sys);
}

static vlc_gl_filter_open_fn Open;
static int
Open(struct vlc_gl_filter *filter, const config_chain_t *config,
     struct vlc_gl_tex_size *size_out) {
    (void) size_out;

    filter->config.filter_planes = false;
    filter->config.blend = false;
    filter->config.msaa_level = 0;
    filter->config.kernel = true;

    struct sys *sys = filter->sys = malloc(sizeof(*sys));
    if (!sys)
        return VLC_EGENERIC;

    config_ChainParse(filter, FILTER_PREFIX, ppsz_filter_options, config);

    atomic_init( &sys->sigma,
                 var_CreateGetFloatCommand( filter, FILTER_PREFIX "sigma" ) );

    var_AddCallback( filter, FILTER_PREFIX "sigma", varFloatCallback, 
    	             &sys->sigma );

    struct vlc_gl_sampler *sampler = vlc_gl_filter_GetSampler(filter);

#ifdef USE_OPENGL_ES2
# define SHADER_VERSION "#version 100\n"
# define FRAGMENT_SHADER_PRECISION "precision highp float;\n"
#else
# define SHADER_VERSION "#version 120\n"
# define FRAGMENT_SHADER_PRECISION
#endif

    static const char *const VERTEX_SHADER =
        SHADER_VERSION
        "attribute vec2 vertex_pos;\n"
        "varying vec2 tex_coords;\n"
        "void main() {\n"
        "  gl_Position = vec4(vertex_pos, 0.0, 1.0);\n"
        "  tex_coords = vec2((vertex_pos.x + 1.0) / 2.0,\n"
        "                    (vertex_pos.y + 1.0) / 2.0);\n"
        "}\n";

    static const char *const FRAGMENT_SHADER_TEMPLATE =
        SHADER_VERSION
        "%s\n" /* extensions */
        FRAGMENT_SHADER_PRECISION
        "%s\n" /* vlc_texture definition */
        "%s\n" /* kernel definition */
	    "varying vec2 tex_coords;\n"
	    "uniform float sigma;\n"
	    "void main() {\n"
        "  vec3 color = vlc_texture_offset(tex_coords, vec2(0.f, 0.f), true).rgb;\n"
        "  vec3 pix = - (vlc_texture_offset(tex_coords, vec2(-1.f, -1.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(0.f, -1.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(1.f, -1.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(-1.f, 0.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(1.f, 0.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(-1.f, 1.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(0.f, 1.f), true).rgb\n"
        "                    + vlc_texture_offset(tex_coords, vec2(1.f, 1.f), true).rgb)\n"
        "                  + 8.f * color;\n"
        "  gl_FragColor = vec4(clamp(color + sigma * pix, 0.f, 1.f), 0.f);\n"
        //"  %s\n"
	    "}\n";
    const char *extensions = sampler->shader.extensions
                           ? sampler->shader.extensions : "";
    // const char *conversion = vlc_fourcc_IsYUV(sampler->fmt.i_chroma)
    //                          ? "  gl_FragColor = ConvMatrix * vec4(color, 1.f);\n"
    //                          : "  gl_FragColor = vec4(color, 1.f);\n";

    char *fragment_shader;
    int ret = asprintf(&fragment_shader, FRAGMENT_SHADER_TEMPLATE, extensions,
                       sampler->shader.body, sampler->shader.kernel);
    if (ret < 0)
        goto error;

    const opengl_vtable_t *vt = &filter->api->vt;

    GLuint program_id =
        vlc_gl_BuildProgram(VLC_OBJECT(filter), vt,
                            1, (const char **) &VERTEX_SHADER,
                            1, (const char **) &fragment_shader);
    
    msg_Info(filter, "%s\n", fragment_shader);
    free(fragment_shader);
    if (!program_id)
        goto error;

    vlc_gl_sampler_FetchLocations(sampler, program_id);

    sys->program_id = program_id;

    sys->loc.vertex_pos = vt->GetAttribLocation(program_id, "vertex_pos");
    assert(sys->loc.vertex_pos != -1);

    sys->loc.sigma = vt->GetUniformLocation(program_id, "sigma");
    assert(sys->loc.sigma != -1);

    vt->GenBuffers(1, &sys->vbo);

    static const GLfloat vertex_pos[] = {
        -1,  1,
        -1, -1,
         1,  1,
         1, -1,
    };

    vt->BindBuffer(GL_ARRAY_BUFFER, sys->vbo);
    vt->BufferData(GL_ARRAY_BUFFER, sizeof(vertex_pos), vertex_pos,
                   GL_STATIC_DRAW);

    static const struct vlc_gl_filter_ops ops = {
        .draw = Draw,
        .close = Close,
    };
    filter->ops = &ops;

    return VLC_SUCCESS;

error:
    free(sys);
    return VLC_EGENERIC;
}

#define SIG_TEXT N_("Sharpen strength (0-2)")
#define SIG_LONGTEXT N_("Set the Sharpen strength, between 0 and 2. Defaults to 0.05.")


DEFINE_OPENGL_EXPORTER( glfilter_sharpen )


vlc_module_begin ()
    set_description("Sharpen opengl video filter")
    set_shortname("glfilter_sharpen")
    set_category( CAT_VIDEO )
    set_subcategory( SUBCAT_VIDEO_VFILTER )
    set_capability( "opengl filter", 0 )
    add_float_with_range( FILTER_PREFIX "sigma", 0.05, 0.0, 2.0,
        SIG_TEXT, SIG_LONGTEXT, false )
    change_safe()
    add_shortcut( "glfilter_sharpen" )
    set_callback(Open)
    add_submodule()
    export_glfilter_as_filter( glfilter_sharpen, 100 )
vlc_module_end ()

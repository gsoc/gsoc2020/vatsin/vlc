/*****************************************************************************
 * posterize.c : Posterize GPU video filter
 *****************************************************************************
 * Copyright (C) 2020 VLC authors and VideoLAN
 * Copyright (C) 2020 Videolabs
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdatomic.h>
#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_modules.h>
#include <vlc_opengl.h>
#include <vlc_filter.h>

#include "../filter.h"
#include "../gl_api.h"
#include "../gl_common.h"
#include "../gl_util.h"

#define CFG_PREFIX "colorthres-"

struct sys {
    GLuint program_id;

    GLuint vbo;

    struct {
        GLint vertex_pos;
        GLint simthres;
        GLint satthres;
        GLint color;
    } loc;
    atomic_int simthres;
    atomic_int satthres;
    atomic_int color;
};

static const char *const ppsz_filter_options[] = {
    "color", "saturationthres", "similaritythres", NULL
};

static int varIntCallback(vlc_object_t *p_this, char const *psz_variable, 
                     vlc_value_t oldvalue, vlc_value_t newvalue, void *p_data) {
    _Atomic int *atom = p_data;
    atomic_store_explicit(atom, newvalue.i_int, memory_order_relaxed);
    (void) p_this; (void) psz_variable; (void) oldvalue;
    return VLC_SUCCESS;
}

static int
Draw(struct vlc_gl_filter *filter, const struct vlc_gl_input_meta *meta)
{
    struct sys *sys = filter->sys;

    const opengl_vtable_t *vt = &filter->api->vt;

    vt->UseProgram(sys->program_id);

    struct vlc_gl_sampler *sampler = vlc_gl_filter_GetSampler(filter);
    vlc_gl_sampler_Load(sampler);

    vt->Uniform1f(sys->loc.simthres, 
                  atomic_load_explicit(&sys->simthres, memory_order_relaxed) / 255.f);
    vt->Uniform1f(sys->loc.satthres, 
                  atomic_load_explicit(&sys->satthres, memory_order_relaxed) / 255.f);
    int rgb = atomic_load_explicit(&sys->color, memory_order_relaxed);
    float b = (rgb % 256) / 255.f;
    rgb >> 8;
    float g = (rgb % 256) / 255.f;
    rgb >> 8;
    float r = (rgb % 256) / 255.f;
    vt->Uniform3f(sys->loc.color, r, g, b);

    vt->BindBuffer(GL_ARRAY_BUFFER, sys->vbo);
    vt->EnableVertexAttribArray(sys->loc.vertex_pos);
    vt->VertexAttribPointer(sys->loc.vertex_pos, 2, GL_FLOAT, GL_FALSE, 0,
                            (const void *) 0);

    vt->Clear(GL_COLOR_BUFFER_BIT);
    vt->DrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    return VLC_SUCCESS;
}

static void
Close(struct vlc_gl_filter *filter) {
    struct sys *sys = filter->sys;

    const opengl_vtable_t *vt = &filter->api->vt;
    vt->DeleteProgram(sys->program_id);
    vt->DeleteBuffers(1, &sys->vbo);
    var_DelCallback( filter, CFG_PREFIX "similaritythres", varIntCallback, 
                     &sys->simthres );
    var_DelCallback( filter, CFG_PREFIX "saturationthres", varIntCallback, 
                     &sys->satthres );
    var_DelCallback( filter, CFG_PREFIX "color", varIntCallback, 
                     &sys->color );

    free(sys);
}

static vlc_gl_filter_open_fn Open;
static int
Open(struct vlc_gl_filter *filter, const config_chain_t *config,
     struct vlc_gl_tex_size *size_out) {

    filter->config.filter_planes = false;
    filter->config.blend = false;
    filter->config.msaa_level = 0;

    struct sys *sys = filter->sys = malloc(sizeof(*sys));
    if (!sys)
        return VLC_EGENERIC;

    config_ChainParse(filter, CFG_PREFIX, ppsz_filter_options, config);

    atomic_init( &sys->simthres,
                 var_CreateGetIntegerCommand( filter, CFG_PREFIX "similaritythres" ) );
    atomic_init( &sys->satthres,
                 var_CreateGetIntegerCommand( filter, CFG_PREFIX "saturationthres" ) );
    atomic_init( &sys->color,
                 var_CreateGetIntegerCommand( filter, CFG_PREFIX "color" ) );

    var_AddCallback( filter, CFG_PREFIX "similaritythres", varIntCallback, 
                     &sys->simthres );
    var_AddCallback( filter, CFG_PREFIX "saturationthres", varIntCallback, 
                     &sys->simthres );
    var_AddCallback( filter, CFG_PREFIX "color", varIntCallback, 
                     &sys->color );

    struct vlc_gl_sampler *sampler = vlc_gl_filter_GetSampler(filter);

#ifdef USE_OPENGL_ES2
# define SHADER_VERSION "#version 100\n"
# define FRAGMENT_SHADER_PRECISION "precision highp float;\n"
#else
# define SHADER_VERSION "#version 120\n"
# define FRAGMENT_SHADER_PRECISION
#endif

    static const char *const VERTEX_SHADER =
        SHADER_VERSION
        "attribute vec2 vertex_pos;\n"
        "varying vec2 tex_coords;\n"
        "void main() {\n"
        "  gl_Position = vec4(vertex_pos, 0.0, 1.0);\n"
        "  tex_coords = vec2((vertex_pos.x + 1.0) / 2.0,\n"
        "                    (vertex_pos.y + 1.0) / 2.0);\n"
        "}\n";

    static const char *const FRAGMENT_SHADER_TEMPLATE =
        SHADER_VERSION
        "%s\n" /* extensions */
        FRAGMENT_SHADER_PRECISION
        "%s\n" /* vlc_texture definition */
        "varying vec2 tex_coords;\n"
        "uniform float simthres;\n"
        "uniform float satthres;\n"
        "uniform vec3 color;\n"
        "\n"
        "void main() {\n"
        "  vec3 rgb = vlc_texture(tex_coords).rgb;\n"
        "  float length = sqrt(rgb.r * rgb.r + rgb.g * rgb.g + rgb.b * rgb.b);\n"
        "  float reflength = sqrt(color.r * color.r + color.g * color.g + color.b * color.b);\n"
        "  float diffr = color.r * length + rgb.r * reflength;\n"
        "  float diffg = color.g * length + rgb.g * reflength;\n"
        "  float diffb = color.b * length + rgb.b * reflength;\n"
        "  float difflen = diffr * diffr + diffg * diffg + diffb * diffb;\n"
        "  float thres = length * reflength;\n"
        "  thres *= thres;\n"
        "  if (length > satthres && difflen * simthres < thres) {\n"
        "    gl_FragColor = vec4(rgb, 1.f);\n"
        "  } else {\n"
        "    gl_FragColor = vec4(0.f);\n"
        "  }"
        "}\n";
    const char *extensions = sampler->shader.extensions
                           ? sampler->shader.extensions : "";

    char *fragment_shader;
    int ret = asprintf(&fragment_shader, FRAGMENT_SHADER_TEMPLATE, extensions,
                       sampler->shader.body);
    if (ret < 0)
        goto error;

    const opengl_vtable_t *vt = &filter->api->vt;

    GLuint program_id =
        vlc_gl_BuildProgram(VLC_OBJECT(filter), vt,
                            1, (const char **) &VERTEX_SHADER,
                            1, (const char **) &fragment_shader);

    free(fragment_shader);
    if (!program_id)
        goto error;

    vlc_gl_sampler_FetchLocations(sampler, program_id);

    sys->program_id = program_id;

    sys->loc.vertex_pos = vt->GetAttribLocation(program_id, "vertex_pos");
    assert(sys->loc.vertex_pos != -1);

    sys->loc.color = vt->GetUniformLocation(program_id, "simthres");
    assert(sys->loc.simthres != -1);

    sys->loc.color = vt->GetUniformLocation(program_id, "satthres");
    assert(sys->loc.satthres != -1);

    sys->loc.color = vt->GetUniformLocation(program_id, "color");
    assert(sys->loc.color != -1);

    vt->GenBuffers(1, &sys->vbo);

    static const GLfloat vertex_pos[] = {
        -1,  1,
        -1, -1,
         1,  1,
         1, -1,
    };

    vt->BindBuffer(GL_ARRAY_BUFFER, sys->vbo);
    vt->BufferData(GL_ARRAY_BUFFER, sizeof(vertex_pos), vertex_pos,
                   GL_STATIC_DRAW);

    static const struct vlc_gl_filter_ops ops = {
        .draw = Draw,
        .close = Close,
    };
    filter->ops = &ops;

    return VLC_SUCCESS;

error:
    free(sys);
    return VLC_EGENERIC;
}


#define COLOR_TEXT N_("Color")
#define COLOR_LONGTEXT N_("Colors similar to this will be kept, others will be "\
    "grayscaled. This must be an hexadecimal (like HTML colors). The first two "\
    "chars are for red, then green, then blue. #000000 = black, #FF0000 = red,"\
    " #00FF00 = green, #FFFF00 = yellow (red + green), #FFFFFF = white" )
#define COLOR_HELP N_("Select one color in the video")
static const int pi_color_values[] = {
  0x00FF0000, 0x00FF00FF, 0x00FFFF00, 0x0000FF00, 0x000000FF, 0x0000FFFF };

static const char *const ppsz_color_descriptions[] = {
  N_("Red"), N_("Fuchsia"), N_("Yellow"), N_("Lime"), N_("Blue"), N_("Aqua") };

vlc_module_begin ()
    set_description( N_("OpenGL Color threshold filter") )
    set_shortname( "glfilter_colorthres")
    set_help(COLOR_HELP)
    set_category( CAT_VIDEO )
    set_subcategory( SUBCAT_VIDEO_VFILTER )
    set_capability( "opengl filter", 0 )
    add_rgb(CFG_PREFIX "color", 0x00FF0000, COLOR_TEXT, COLOR_LONGTEXT)
        change_integer_list( pi_color_values, ppsz_color_descriptions )
    add_integer( CFG_PREFIX "saturationthres", 20,
                 N_("Saturation threshold"), "", false )
    add_integer( CFG_PREFIX "similaritythres", 15,
                 N_("Similarity threshold"), "", false )
    set_callback( Open )
vlc_module_end ()


